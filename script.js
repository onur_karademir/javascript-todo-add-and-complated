const form = document.getElementById("form");
const input = document.getElementById("input");
const todosUL = document.getElementById("todos");

const lsData = JSON.parse(localStorage.getItem("todolist"));

if (lsData) {
    lsData.forEach(item => {
        addMyTodo(item)
    });
}

function addMyTodo(item) {
    
    let todoValue = input.value;

    if (item) {
        todoValue = item.Ttodo
    }

    if (todoValue) {
        const todoElement = document.createElement("li");
        if (item && item.completed) {
            todoElement.classList.add("completed");
        }
        todoElement.innerText = todoValue;
        
        todoElement.addEventListener('click', () => {
            todoElement.classList.toggle("completed")
            updateLS()
        });

        todoElement.addEventListener('contextmenu', (e) => {
            e.preventDefault();
            todoElement.remove();
            updateLS()
        });

        
        todosUL.appendChild(todoElement);
        input.value = ''

        updateLS()
        
    }
}

form.addEventListener('submit', (e) => {
    e.preventDefault();
    addMyTodo();
});

function updateLS() {

    const todoElement = document.querySelectorAll("li");
    
    const todoArray = [];

    todoElement.forEach(myTodo => {
        todoArray.push({
            Ttodo : myTodo.innerText,
            completed : myTodo.classList.contains("completed"),
        });    
    });
    
    localStorage.setItem("todolist",JSON.stringify(todoArray))
}

